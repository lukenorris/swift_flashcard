//
//  ViewController.swift
//  flashcards
//
//  Created by Luke Norris on 2/3/21.
//  Copyright © 2021 Luke Norris. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var flashCardArray: Array<Flashcard> = []
    var flashCardSet: Array<Flashcardset> = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        flashCardArray = returnFlashcardArray()
        flashCardSet = returnFlashcardSet()
    }


}

