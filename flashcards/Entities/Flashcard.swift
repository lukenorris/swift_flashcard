//
//  Flashcard.swift
//  flashcards
//
//  Created by Luke Norris on 2/3/21.
//  Copyright © 2021 Luke Norris. All rights reserved.
//

import Foundation

class Flashcard{
    var term: String
    var definition: String
    
    init(term: String, definition: String){
        self.term = term
        self.definition = definition
    }
    func getCard()->String{
        return "term: "+(term)+" definition: "+(definition)
    }
    
}
var card1:Flashcard = Flashcard(term:"1**2",definition:"1")
var card2:Flashcard = Flashcard(term:"2**2",definition:"4")
var card3:Flashcard = Flashcard(term:"3**2",definition:"9")
var card4:Flashcard = Flashcard(term:"4**2",definition:"16")
var card5:Flashcard = Flashcard(term:"5**2",definition:"25")
var card6:Flashcard = Flashcard(term:"6**2",definition:"36")
var card7:Flashcard = Flashcard(term:"7**2",definition:"49")
var card8:Flashcard = Flashcard(term:"8**2",definition:"64")
var card9:Flashcard = Flashcard(term:"9**2",definition:"81")
var card10:Flashcard = Flashcard(term:"10**2",definition:"100")

let FlashcardArray = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10]
func returnFlashcardArray()->Array<Flashcard>{
    //print(card1.getCard())
    return FlashcardArray
}

