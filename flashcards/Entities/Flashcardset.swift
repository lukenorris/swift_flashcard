//
//  Flashcardset.swift
//  flashcards
//
//  Created by Luke Norris on 2/3/21.
//  Copyright © 2021 Luke Norris. All rights reserved.
//

import Foundation

class Flashcardset{
    var Title: String
    
    init(Title: String){
        self.Title = Title
    }
}
var set1:Flashcardset = Flashcardset(Title:"set1")
var set2:Flashcardset = Flashcardset(Title:"set2")
var set3:Flashcardset = Flashcardset(Title:"set3")
var set4:Flashcardset = Flashcardset(Title:"set4")
var set5:Flashcardset = Flashcardset(Title:"set5")
var set6:Flashcardset = Flashcardset(Title:"set6")
var set7:Flashcardset = Flashcardset(Title:"set7")
var set8:Flashcardset = Flashcardset(Title:"set8")
var set9:Flashcardset = Flashcardset(Title:"set9")
var set10:Flashcardset = Flashcardset(Title:"set10")

let FlashcardSet = [set1,set2,set3,set4,set5,set6,set7,set8,set9,set10]
func returnFlashcardSet()->Array<Flashcardset>{
    return FlashcardSet
}
